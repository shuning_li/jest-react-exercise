import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent, waitForElement } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  // Mock数据请求
  // 触发事件
  // 给出断言
  const { getByTestId, getByLabelText } = render(<OrderComponent />);
  const input = getByLabelText('number-input');
  const button = getByLabelText('submit-button');

  fireEvent.change(input, { target: { value: '123456' } });

  axios.get.mockResolvedValueOnce({
    data: { status: '已完成' }
  });

  fireEvent.click(button);

  const status = await waitForElement(() => getByTestId('status'));

  expect(axios.get).toHaveBeenCalledTimes(1);
  expect(axios.get).toHaveBeenCalledWith('/order/123456');
  expect(status).toHaveTextContent('已完成');
  // --end->
});
